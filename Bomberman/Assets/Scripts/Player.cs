﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
	private float FLOOR_Y = 0.5f;

	[SerializeField]
	private float speed = 10;
	[SerializeField]
	private float jumpStrength = 15;

	private float vy = 0;

	void Start () {
		
	}

	void Update () {
		Vector3 move = Vector3.zero;

		if (Input.GetKey(KeyCode.RightArrow))
			move += Vector3.right * speed;
		if (Input.GetKey(KeyCode.LeftArrow))
			move += Vector3.left * speed;
		if (Input.GetKey(KeyCode.UpArrow))
			move += Vector3.forward * speed;
		if (Input.GetKey(KeyCode.DownArrow))
			move += Vector3.back * speed;
		if (Input.GetKey (KeyCode.Space) && transform.position.y == FLOOR_Y)
			vy = jumpStrength;

		move += Vector3.up * vy;

		transform.Translate (move * Time.deltaTime);

		if (transform.position.y < FLOOR_Y) {
			transform.Translate (0, -transform.position.y + FLOOR_Y, 0);
			vy = 0;
		} else
			vy -= 1;
	}
}
